# About

This is the repository for the emzed3 demo workflow.


## Development setup

1. Create a virtualenvironment named `venv` in the same folder as this README file.
2. Activate the environment.
3. Run `pip install -r setup/requirements.txt -r setup/requirements_dev.txt`
4. Run `pre-commit install`

This will enable all [pre-commit](https://pre-commit.com/) hooks which:

- run black and isort on the affected notebooks
- clear notebook output cells and remove meta data to reduce diffs in merge requests
- run a spell checker on the markdown and html content of notebooks

In case `pre-commit` reformats the notebook or removes output cells, you must commit again.

In case the spellchecker detects misspelled words, you must either fix them in the notebook
or (if appropriate) update the `spellchecker_whitelist.txt` file.
