#!/usr/bin/env python

import json
import os
import sys

from collections import UserDict

from nbclient import NotebookClient

"""
1. executes cell with tag "init-cell"
2. clears output for all other cell
3. removes ids
"""


class DD(UserDict):
    __getattr__ = UserDict.__getitem__


def fix(dd):
    if isinstance(dd, dict):
        items = fix(list(dd.items()))
        return DD(items)
    if isinstance(dd, (tuple, list, set)):
        values = []
        for item in dd:
            values.append(fix(item))
        if isinstance(dd, tuple):
            return tuple(values)
        if isinstance(dd, set):
            return set(values)
        return values
    return dd


class DDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, DD):
            return dict(obj)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


for p in sys.argv[1:]:
    if not p.endswith(".ipynb"):
        continue

    with open(p) as fh:
        print(p)
        nb = fix(json.load(fh))

    nb.metadata = {}

    client = NotebookClient(
        nb, timeout=600, resources={"metadata": {"path": os.path.dirname(p)}}
    )
    with client.setup_kernel():
        for i, cell in enumerate(nb["cells"]):
            cell.pop("id", None)
            if cell["cell_type"] == "code":
                cell["outputs"].clear()
                cell["execution_count"] = 0
    print("WRITE", p)
    with open(p, "w") as fh:
        json.dump(nb, fh, indent=2, cls=DDEncoder)
